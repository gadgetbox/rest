# GadgetBox.Rest

This is ASP.NET Core project. It provides RESTful API for management device.

## Requirements

* ASP.NET Core 2.1
* Docker Version 2.0.0.3 (31259)

## Configuration

> For configuring application use environment variables for **Production** and appsettings.Development.json for **Development**

 For sensitive data use *user secrets*.

> The JSON structure is flattened after modifications via dotnet user-secrets remove or dotnet user-secrets set. For example, running dotnet user-secrets remove "Movies:ConnectionString" collapses the Movies object literal.
 
 ```bash
 dotnet user-secrets set "Movies:ServiceApiKey" "12345"
 ```

### DeviceHive

***Root element:*** DeviceHive.

> For quick start use [DeviceHive Playground](https://playground.devicehive.com/). It is free cloud hosting.

|Param Name     |Description                    |
|---------------|-------------------------------|
|DeviceId       |Unique device name that was given to device on step of configuration firmware|
|Rest           |DeviceHive rest hosting `https://HOST/api/rest`|
|RefreshToken   |**Secret** token for access. *Use user secrets for setting*|

## Build

### Build console

```bash
cd src/GadgetBox.Rest/
dotnet build
```

### Build Docker container

```bash
cd src/
docker build -t gadgetbox.rest -f GadgetBox.Rest/Dockerfile .
docker run -d -p 8080:80 gadgetbox.rest
```

## Artifacts

For build artifacts used [gitlab container registry](https://gitlab.com/gadgetbox/rest/container_registry). 

### Use

```bash
docker login registry.gitlab.com
docker pull registry.gitlab.com/gadgetbox/rest:{tag}
docker run -d -p 8080:80 registry.gitlab.com/gadgetbox/rest:{tag}
```

### Tags

|Tag Label|Type|Description|
|---------|----|-----------|
|*latest*|**Production**|build from master branch|
|*release-**|**RC**|build from release/* branches|
|*develop*|**ALPHA**|build from develop branch|
|*feature-**|**Feature**|build from feature/* branches|
