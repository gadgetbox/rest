﻿using System;
using System.Diagnostics.Contracts;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace GadgetBox.DeviceHive
{
    public class TokenAuthenticatedHandler : DelegatingHandler
    {
        private readonly SemaphoreSlim _lock = new SemaphoreSlim(1, 1);
        private string _currentToken;
        private string _refreshToken;
        private readonly string _serviceUrl;

        public TokenAuthenticatedHandler(
            string refreshToken,
            string serviceUrl)
        {
            if (String.IsNullOrWhiteSpace(refreshToken))
            {
                throw new ArgumentException(
                    "<refreshToken> cannot be null or white space.",
                    nameof(refreshToken));
            }

            _refreshToken = refreshToken;
            _serviceUrl = serviceUrl;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (String.IsNullOrWhiteSpace(_currentToken))
            {
                await RefreshToken()
                    .ConfigureAwait(false);
            }

            SetAuthData(request, _currentToken);


            var result = await base.SendAsync(request, cancellationToken)
                .ConfigureAwait(false);

            bool newTokenRequired = !result.IsSuccessStatusCode
                && (result.StatusCode == System.Net.HttpStatusCode.Unauthorized);

            if (!newTokenRequired)
            {
                return result;
            }

            await RefreshToken()
                .ConfigureAwait(false);
            SetAuthData(request, _currentToken);

            result = await base.SendAsync(request, cancellationToken)
                .ConfigureAwait(false);

            return result;
        }

        private async Task RefreshToken()
        {
            _currentToken = null;
            await _lock.WaitAsync()
                .ConfigureAwait(false);
            try
            {
                if (!String.IsNullOrWhiteSpace(_currentToken))
                {
                    return;
                }

                var refreshRequest = new HttpRequestMessage(
                    HttpMethod.Post,
                    _serviceUrl + "token/refresh");

                refreshRequest.Headers.Authorization = null;
                refreshRequest.Content = new StringContent(
                    $"{{\"refreshToken\":\"{_refreshToken}\"}}",
                    System.Text.Encoding.UTF8,
                    "application/json");

                var refreshResponse = await base.SendAsync(
                    refreshRequest,
                    CancellationToken.None)
                    .ConfigureAwait(false);

                refreshResponse.EnsureSuccessStatusCode();

                var body = await refreshResponse
                    .Content
                    .ReadAsStringAsync()
                    .ConfigureAwait(false);

                _currentToken = JToken.Parse(body).Value<string>("accessToken");
            }
            finally
            {
                _lock.Release();
            }
        }

        private static void SetAuthData(HttpRequestMessage request, String currentToken)
        {
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", currentToken);
        }
    }
}
