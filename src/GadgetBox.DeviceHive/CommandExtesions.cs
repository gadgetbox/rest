﻿using System;
using DeviceHive.Client;

namespace GadgetBox.DeviceHive
{
    public static class CommandExtesions
    {
        public static bool IsDeviceAvailable(this Command command)
        {
            return !string.IsNullOrWhiteSpace(command.Status)
                && command.Result != null;
        }
    }
}
