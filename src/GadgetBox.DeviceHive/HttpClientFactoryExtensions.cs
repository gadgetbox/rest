﻿using System;
using System.Net.Http.Headers;
using DeviceHive.Client;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace GadgetBox.DeviceHive
{
    public static class HttpClientFactoryExtensions
    {
        public static IHttpClientBuilder AddDeviceHiveClient(this IServiceCollection services, DeviceHiveConnectionInfo connectionInfo)
        {
            services.AddSingleton(connectionInfo);

            var builder = services.AddHttpClient(nameof(IRestClient))
                .ConfigureHttpClient((sp, httpClient) =>
                {
                    var options = sp.GetService<DeviceHiveConnectionInfo>();

                    RestClient.ConfigureHttpClient(httpClient, options);
                })
                .AddTypedClient<IRestClient, RestClient>();


            services.AddSingleton<IDeviceHiveClient>(sp =>
            {
                var options = sp.GetService<DeviceHiveConnectionInfo>();
                var client = sp.GetService<IRestClient>();

                var dh = new DeviceHiveClient(options, client);

                dh.SetAvailableChannels(new[]
                {
                    // create build for channels
                    new LongPollingChannel(options, client)
                });

                return dh;
            });

            return  builder;
        }
    }
}
