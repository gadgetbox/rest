﻿using System;
namespace GadgetBox.Rest.Features.IoT
{
    public class CommandResult
    {
        public ExecutionsResult Status { get; set; }
        public String Message { get; internal set; }
    }
}
