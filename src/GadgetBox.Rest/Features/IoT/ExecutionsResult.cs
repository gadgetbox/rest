﻿
namespace GadgetBox.Rest.Features.IoT
{
    public enum ExecutionsResult
    {
        DeviceDisconnect,
        OK,
        Error
    }
}