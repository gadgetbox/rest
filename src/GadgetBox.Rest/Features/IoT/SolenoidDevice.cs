﻿using DeviceHive.Client;
using GadgetBox.DeviceHive;
using Newtonsoft.Json.Linq;
using System;
using System.Threading.Tasks;

namespace GadgetBox.Rest.Features.IoT
{
    public class SolenoidDevice
    {
        public SolenoidDevice(string deviceId, IDeviceHiveClient deviceHiveClient)
        {
            _deviceId = deviceId;
            _deviceHiveClient = deviceHiveClient;
        }
        /// <summary>
        /// Open solenoid
        /// </summary>
        /// <param name="pinNumber">GPIO pin number</param>
        /// <returns></returns>
        public Task<ReadResult> OpenAsync(int pinNumber) => ChangeSolenoidStatus(pinNumber, false);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pinNumber">GPIO pin number</param>
        /// <returns></returns>
        public Task<ReadResult> CloseAsync(int pinNumber) => ChangeSolenoidStatus(pinNumber, true);
        /// <summary>
        /// Close solenoid
        /// </summary>
        /// <param name="pinNumber">GPIO pin number</param>
        /// <param name="isClosed">0 open, 1 close</param>
        /// <returns></returns>
        private async Task<ReadResult> ChangeSolenoidStatus(int pinNumber, bool isClosed)
        {
            var command = await _deviceHiveClient.SendCommandAsync(
                    _deviceId,
                    new PinWriteCommand()
                    {
                        Parameters = JToken.Parse($"{{\"{pinNumber}\":\"{(isClosed ? 1 : 0)}\"}}")
                    })
                .ConfigureAwait(false);

            var readCommand = await _deviceHiveClient.WaitCommandResultAsync(
                    _deviceId,
                    command.Id.Value,
                    null)
                .ConfigureAwait(false);


            if (!readCommand.IsDeviceAvailable())
            {
                return new ReadResult
                {
                    Status = ExecutionsResult.DeviceDisconnect,
                    Message = "Device disconnect from DeviceHive"
                };
            }

            var status = Enum.Parse<ExecutionsResult>(readCommand.Status);

            if (status == ExecutionsResult.Error)
            {
                return new ReadResult
                {
                    Status = status,
                    Message = readCommand.Result.ToString()
                };
            }

            return new ReadResult
            {
                Status = status,
                Type = readCommand.Result.Value<string>("type"),
                Uid = readCommand.Result.Value<string>("uid"),
            };
        }

        private readonly string _deviceId;
        private readonly IDeviceHiveClient _deviceHiveClient;
    }
}
