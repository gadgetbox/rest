﻿using DeviceHive.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GadgetBox.Rest.Features.IoT
{
    public class PinWriteCommand : Command
    {
        public PinWriteCommand() : base("gpio/write")
        {
            
        } 
    }
}
