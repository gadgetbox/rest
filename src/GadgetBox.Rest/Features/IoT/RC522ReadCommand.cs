﻿using System;
using DeviceHive.Client;

namespace GadgetBox.Rest.Features.IoT
{
    public class RC522ReadCommand : Command
    {
        //public RC522ReadCommand()
        //    : base("devices/mfrc522/mifare/read")
        //{
        //}
        public RC522ReadCommand()
           : base("devices/mfrc522/read")
        {
        }
        public int CS
        {
            get => GetParameter<int>(nameof(CS));
            set => Parameter<int>(nameof(CS), value);
        }
    }
}
