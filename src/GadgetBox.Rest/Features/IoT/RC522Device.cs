﻿using System;
using System.Text;
using System.Threading.Tasks;
using DeviceHive.Client;
using GadgetBox.DeviceHive;
using Newtonsoft.Json.Linq;

namespace GadgetBox.Rest.Features.IoT
{
    public class RC522Device
    {
        public RC522Device(string deviceId, IDeviceHiveClient deviceHiveClient)
        {
            _deviceId = deviceId;
            _deviceHiveClient = deviceHiveClient;
        }

        public async Task<ReadResult> ReadAsync()
        {
            var command = await _deviceHiveClient.SendCommandAsync(
                _deviceId,
                //new RC522ReadCommand()
                //{
                //    CS = 2,
                //}
                new Command(
                    "devices/mfrc522/mifare/read",
                    JToken.FromObject(new
                    {
                        CS = 2,
                        address = "0x04"
                    }))
                )
                .ConfigureAwait(false);

            var readCommand = await _deviceHiveClient.WaitCommandResultAsync(
                _deviceId,
                command.Id.Value,
                null)
                .ConfigureAwait(false);


            if (!readCommand.IsDeviceAvailable())
            {
                return new ReadResult
                {
                    Status = ExecutionsResult.DeviceDisconnect,
                    Message = "Device disconnect from DeviceHive"
                };
            }

            var status = Enum.Parse<ExecutionsResult>(readCommand.Status);

            if (status == ExecutionsResult.Error)
            {
                return new ReadResult
                {
                    Status = status,
                    Message = readCommand.Result.ToString()
                };
            }

            return new ReadResult
            {
                Status = status,
                Type = readCommand.Result.Value<string>("type"),
                Uid = readCommand.Result.Value<string>("uid"),
                //Data = Base64Decode(readCommand.Result.Value<string>("data")),
                Data = readCommand.Result.Value<string>("data")
            };
        }
        private string Base64Decode(string data)
        {
            try
            {
                System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
                System.Text.Decoder utf8Decode = encoder.GetDecoder();

                byte[] todecode_byte = Convert.FromBase64String(data);
                int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
                char[] decoded_char = new char[charCount];
                utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
                string result = new String(decoded_char);
                return result;
            }
            catch (Exception e)
            {
                throw new Exception("Error in base64Decode" + e.Message);
            }
        }
        private readonly string _deviceId;
        private readonly IDeviceHiveClient _deviceHiveClient;
    }
}
