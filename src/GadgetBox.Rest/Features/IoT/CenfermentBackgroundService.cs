﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GadgetBox.Rest.Infrastructure.DB;
using GadgetBox.Rest.Infrastructure.Enums;
using GadgetBox.Rest.Infrastructure.Models;
using GadgetBox.Rest.Infrastructure.Models.Extensions;
using GadgetBox.Rest.Infrastructure.Options;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace GadgetBox.Rest.Features.IoT
{
    public class CenfermentBackgroundService : IHostedService
    {
        public TimeSpan CheckForActionTime { get; set; } = TimeSpan.FromSeconds(30);
        
        public ILogger<CenfermentBackgroundService> Logger { get; set; }
        public TimeSpan PollTime { get; set; } = TimeSpan.FromSeconds(2);

        public CenfermentBackgroundService(
            RC522Device rc522Device,
            IServiceScopeFactory serviceScopeFactory,
            SolenoidDevice solenoidDevice,
            DeviceSemaphoreConfig semaphoreConfig)
        {
            _lock = semaphoreConfig.Semaphore;
            _cts = new CancellationTokenSource();

            _rc522Device = rc522Device;
            _serviceScopeFactory = serviceScopeFactory;
            _solenoidDevice = solenoidDevice;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Task.Run(StartRC522LoopAsync, _cts.Token);

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _cts.Cancel();
            return Task.CompletedTask;
        }

        public void StartCheckPlayground()
        {
            _lock.Release();
        }

        private async Task StartRC522LoopAsync()
        {
            while (!_cts.IsCancellationRequested)
            {
                _lock.WaitOne();
                try
                {
                    var time = CheckForActionTime;
                    while ((time = time - PollTime) > TimeSpan.FromSeconds(0) && !_cts.IsCancellationRequested)
                    {
                        var readResult = await _rc522Device
                            .ReadAsync()
                            .ConfigureAwait(false);

                        LogReadResult(readResult);

                        if (readResult.Status == ExecutionsResult.OK)
                        {
                            await ProcessReadResult(readResult)
                                .ConfigureAwait(false);
                        }
                        else
                        {
                            Logger?.LogDebug("Error from device hive");
                        }

                        await Task.Delay(PollTime)
                            .ConfigureAwait(false);
                    }
                }
                catch (Exception ex)
                {
                    Logger?.LogError(ex, "RC522 reading failed");
                    throw;
                }
            }
        }

        private void LogReadResult(ReadResult readResult)
        {
            var sb = new StringBuilder();
            sb.AppendLine(new string('-', 10));
            sb.AppendLine("RC522 read");
            sb.Append("Timestamp: ").AppendLine(DateTimeOffset.Now.ToString());
            sb.Append("Status: ").AppendLine(readResult.Status.ToString("G"));
            sb.Append("UID: ").AppendLine(readResult.Uid);
            sb.Append("Type: ").AppendLine(readResult.Type);
            sb.Append("Data: ").AppendLine(readResult.Data);
            sb.AppendLine("Message: ").AppendLine(readResult.Message);
            sb.AppendLine(new string('-', 10));

            Logger?.LogDebug(sb.ToString());
        }

        private async Task ProcessReadResult(ReadResult readResult)
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var bookingConfig = scope.ServiceProvider.GetService<IOptions<BookingConfig>>();
                var userRepo = scope.ServiceProvider.GetService<IRepository<User>>();
                var deviceRepo = scope.ServiceProvider.GetService<IRepository<Device>>();
                var deviceHistoryRepo = scope.ServiceProvider.GetService<IRepository<History>>();

                var readedCardCode = readResult.Data.Trim();

                var users = await userRepo.GetAllAsync()
                    .ConfigureAwait(false);
                var user = users.FirstOrDefault(x => x.CardCode == readedCardCode);
                if (user == null)
                {
                    Logger?.LogInformation("User with card number not found");
                    return;
                }

                var devices = await deviceRepo.GetAllAsync()
                    .ConfigureAwait(false);
                var device = devices.FirstOrDefault(x => x.OwnerId == user.Id);

                if (device == null)
                {
                    Logger?.LogInformation("Device not found");
                    return;
                }
                bool isActionExecuted = false;
                if (device.IsBooked(bookingConfig.Value.BookingTimeInSeconds))
                {
                    device.OwnerId = user.Id;
                    device.Status = ReservationStatus.InUse;
                    await deviceRepo.UpdateAsync(device)
                        .ConfigureAwait(false);

                    var inUseHistoryItem = History.Create(user.Id, device.Id, ReservationStatus.InUse.ToString());
                    await deviceHistoryRepo.AddAsync(inUseHistoryItem)
                        .ConfigureAwait(false);
                    isActionExecuted = true;
                }

                if (device.Status == ReservationStatus.WaitForReturning)
                {
                    device.OwnerId = null;
                    device.Status = ReservationStatus.Free;
                    await deviceRepo.UpdateAsync(device)
                        .ConfigureAwait(false);

                    var inUseHistoryItem = History.Create(user.Id, device.Id, ReservationStatus.Free.ToString());
                    await deviceHistoryRepo.AddAsync(inUseHistoryItem)
                        .ConfigureAwait(false);
                    isActionExecuted = true;
                }

                if (isActionExecuted && _boxPinMapper.TryGetValue(device.SlotIndex, out var pinNumber))
                {
                    try
                    {
                        readResult = await _solenoidDevice.OpenAsync(pinNumber)
                            .ConfigureAwait(false);

                        CloseAfterTimeout(pinNumber, bookingConfig.Value.OpenTimeInSeconds);

                        LogReadResult(readResult);
                    }
                    catch (Exception ex)
                    {
                        Logger?.LogError(ex, "Solenoid reading failed");
                        throw;
                    }
                }
            }
        }

        private void CloseAfterTimeout(int pinNumber, Int32 delayInSeconds)
        {
            Task.Factory.StartNew(async () =>
            {
                await Task.Delay(TimeSpan.FromSeconds(delayInSeconds));
                await _solenoidDevice.CloseAsync(pinNumber)
                    .ConfigureAwait(false);
                Logger?.LogInformation("Solenoid is closing");
            });

        }

        private Dictionary<int, int> _boxPinMapper = new Dictionary<int, int>
        {
            { 0, 4 },//ESP pin 2
            { 1, 14 }//ESP pin 5
        };

        private readonly Semaphore _lock;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        private readonly CancellationTokenSource _cts;
        private readonly RC522Device _rc522Device;
        private readonly SolenoidDevice _solenoidDevice;
    }
}
