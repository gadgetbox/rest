﻿using System;
using Newtonsoft.Json.Linq;

namespace GadgetBox.Rest.Features.IoT
{
    public class ReadResult : CommandResult
    {
        public string Uid { get; set; }
        public string Type { get; set; }
        public string Data { get; set; }
    }
}
