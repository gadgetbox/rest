using System;
using System.Threading.Tasks;
using GadgetBox.Rest.Infrastructure.API;
using GadgetBox.Rest.Infrastructure.DB;
using GadgetBox.Rest.Infrastructure.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading;
using Microsoft.Extensions.Options;
using GadgetBox.Rest.Infrastructure.Options;
using GadgetBox.Rest.Infrastructure.Enums;
using GadgetBox.Rest.Infrastructure.Models.Extensions;

namespace GadgetBox.Rest.Features.Devices
{
    public class DevicesController : ApiControllerBase
    {
        public DevicesController(
            IRepository<Device> devicesRepo,
            IRepository<User> usersRepo,
            IRepository<History> devicesHistoryRepo,
            IOptions<BookingConfig> bookingConfig,
            DeviceSemaphoreConfig semaphoreConfig)
        {
            _devicesRepo = devicesRepo;
            _usersRepo = usersRepo;
            _devicesHistoryRepo = devicesHistoryRepo;
            _bookingConfig = bookingConfig.Value;
            _semaphore = semaphoreConfig.Semaphore;
        }

        [HttpGet]
        [Route("")]
        public async Task<object> GetAll()
        {
            var devices = await _devicesRepo.GetAllAsync()
                .ConfigureAwait(false);

            return devices.Select(x => new
            {
                x.Id,
                x.Label,
                IsBooked = x.IsBooked(_bookingConfig.BookingTimeInSeconds),
                OwnerId = !x.IsUsed() || x.IsBooked(_bookingConfig.BookingTimeInSeconds) ? x.OwnerId : null,
                x.SlotIndex,
                x.BoxId,
                x.DeviceType,
                GetDeviceOs = x.DeviceOs,
                IsUsed = x.IsUsed()
            });
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<object> GetDeviceById(Int32 id)
        {
            var device = await _devicesRepo.GetAsync(id)
                .ConfigureAwait(false);

            if (device == null)
            {
                return BadRequest("Device not found");
            }

            return new
            {
                device.Id,
                device.Label,
                device.OwnerId,
                device.SlotIndex,
                device.BoxId,
                device.DeviceType,
                GetDeviceOs = device.DeviceOs,
                IsBooked = device.IsBooked(_bookingConfig.BookingTimeInSeconds),
                IsUsed = device.IsUsed()
            };
        }

        [HttpGet]
        [Route("{id}/history")]
        public async Task<History[]> GetDeviceHistory(Int32 id)
        {
            var allDevicesHistory = await _devicesHistoryRepo.GetAllAsync()
                 .ConfigureAwait(false);

            var deviceHistory = allDevicesHistory.Where(h => h.DeviceId == id)
                .OrderByDescending(x => x.TimeUtc)
                .ToArray();

            return deviceHistory;
        }

        [HttpPut]
        [Route("{id}/booking/users/current")]
        public async Task<IActionResult> SetCurrentUser(
            [FromRoute]int id,
            [FromBody]Conferment conferment)
        {
            var tasks = new Task[]
            {
                _devicesRepo.GetAsync(id),
                _usersRepo.GetAsync(conferment.UserId),
            };

            await Task.WhenAll(tasks)
                .ConfigureAwait(false);

            var device = ((Task<Device>)tasks[0]).Result;
            var user = ((Task<User>)tasks[1]).Result;

            if (device == null || user == null)
            {
                return BadRequest();
            }

            if (device.IsBooked(_bookingConfig.BookingTimeInSeconds))
            {
                return BadRequest("Device is booked");
            }

            if (device.IsUsed())
            {
                return BadRequest("Device is used");
            }

            device.TimeUtc = DateTime.UtcNow;
            device.Status = ReservationStatus.Booked;
            device.OwnerId = user.Id;
            await _devicesRepo.UpdateAsync(device)
                .ConfigureAwait(false);

            var newHistoryItem = History.Create(user.Id, device.Id, ReservationStatus.Booked.ToString()); 
            await _devicesHistoryRepo.AddAsync(newHistoryItem)
                .ConfigureAwait(false);

            _semaphore.Release();

            return NoContent();
        }

        [HttpPut]
        [Route("{id}/return/users/current")]
        public async Task<IActionResult> ReturnDeviceAsync(
            [FromRoute]int id,
            [FromBody]Conferment conferment)
        {
            var tasks = new Task[]
            {
                _devicesRepo.GetAsync(id),
                _usersRepo.GetAsync(conferment.UserId),
            };

            await Task.WhenAll(tasks)
                .ConfigureAwait(false);

            var device = ((Task<Device>)tasks[0]).Result;
            var user = ((Task<User>)tasks[1]).Result;

            if (device == null || user == null)
            {
                return BadRequest();
            }

            if (!device.IsUsed())
            {
                return BadRequest("Device not used");
            }

            device.TimeUtc = DateTime.UtcNow;
            device.Status = ReservationStatus.WaitForReturning;
            await _devicesRepo.UpdateAsync(device)
                .ConfigureAwait(false);

            var newHistoryItem = History.Create(user.Id, device.Id, ReservationStatus.WaitForReturning.ToString());
            await _devicesHistoryRepo.AddAsync(newHistoryItem)
                .ConfigureAwait(false);

            _semaphore.Release();

            return NoContent();
        }

        [HttpDelete]
        [Route("{id}/return/users/admin")]
        public async Task<IActionResult> ReturnDeviceByAdmin(
            [FromRoute]int id)
        {
            var device = await _devicesRepo.GetAsync(id)
                .ConfigureAwait(false);

            if (device?.OwnerId == null)
            {
                return BadRequest();
            }

            var uid = device.OwnerId.Value;
            device.OwnerId = null;
            device.Status = ReservationStatus.Free;

            await _devicesRepo.UpdateAsync(device)
               .ConfigureAwait(false);

            var newHistoryItem = History.Create(uid, device.Id, ReservationStatus.Free.ToString());
            await _devicesHistoryRepo.AddAsync(newHistoryItem)
                .ConfigureAwait(false);

            return NoContent();
        }

        [HttpPut]
        [Route("{id}/booking/users/admin")]
        public async Task<IActionResult> SetCurrentUserByAdmin(
            [FromRoute]int id,
            [FromBody]Conferment conferment)
        {
            var tasks = new Task[]
            {
                _devicesRepo.GetAsync(id),
                _usersRepo.GetAsync(conferment.UserId),
                GetDeviceHistory(id)
            };

            await Task.WhenAll(tasks)
                .ConfigureAwait(false);

            var device = ((Task<Device>)tasks[0]).Result;
            var user = ((Task<User>)tasks[1]).Result;

            if (device == null || user == null)
            {
                return BadRequest();
            }

            device.OwnerId = user.Id;
            device.Status = ReservationStatus.InUse;
            await _devicesRepo.UpdateAsync(device)
                .ConfigureAwait(false);

            var newHistoryItem = History.Create(user.Id, device.Id, ReservationStatus.InUse.ToString());
            await _devicesHistoryRepo.AddAsync(newHistoryItem)
                .ConfigureAwait(false);

            return NoContent();
        }

        private readonly Semaphore _semaphore;

        private readonly IRepository<Device> _devicesRepo;
        private readonly IRepository<User> _usersRepo;
        private readonly IRepository<History> _devicesHistoryRepo;
        private readonly BookingConfig _bookingConfig;
    }
}
