﻿using System;
using GadgetBox.Rest.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GadgetBox.Rest.Features.EF.Mappings
{
    public class BoxConfiguration : IEntityTypeConfiguration<Box>
    {
        public void Configure(EntityTypeBuilder<Box> builder)
        {
            builder.HasKey(m => m.Id);

            builder.Property(m => m.SlotsAmount);
        }
    }
}
