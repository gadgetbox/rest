﻿using System;
using GadgetBox.Rest.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GadgetBox.Rest.Features.EF.Mappings
{
    public class DeviceCofiguration : IEntityTypeConfiguration<Device>
    {
        public void Configure(EntityTypeBuilder<Device> builder)
        {
            builder.HasKey(m => m.Id);

            builder.Property(m => m.Label);
            builder.HasOne<User>()
                .WithMany()
                .HasForeignKey(m => m.OwnerId);
            builder.HasOne<Box>()
                .WithMany()
                .HasForeignKey(m => m.BoxId);
        }
    }
}
