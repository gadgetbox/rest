﻿using GadgetBox.Rest.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GadgetBox.Rest.Features.EF.Mappings
{
    public class HistoryConfiguration : IEntityTypeConfiguration<History>
    {
        public void Configure(EntityTypeBuilder<History> builder)
        {
            builder.HasKey(m => m.Id);

            builder.Property(m => m.Action);

            builder.Property(m => m.TimeUtc);
            builder.HasOne<User>()
                .WithMany()
                .HasForeignKey(m => m.UserId);

            builder.HasOne<Device>()
                .WithMany()
                .HasForeignKey(m => m.DeviceId);
        }
    }
}
