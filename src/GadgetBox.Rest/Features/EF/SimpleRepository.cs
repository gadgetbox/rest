﻿using System;
using System.Threading.Tasks;
using GadgetBox.Rest.Infrastructure.DB;
using Microsoft.EntityFrameworkCore;

namespace GadgetBox.Rest.Features.EF
{
    public class SimpleRepository<T> : IRepository<T>
        where T : class, IModel
    {
        public SimpleRepository(DbContext dbContext)
        {
            _dbContex = dbContext;
            _set = _dbContex.Set<T>();
        }


        public Task<T[]> GetAllAsync() => _set.AsNoTracking().ToArrayAsync();

        public Task<T> GetAsync(int id) 
            => _set.AsNoTracking().FirstOrDefaultAsync(m => m.Id == id);

        public async Task UpdateAsync(T entity)
        {
            _set.Update(entity);
            await _dbContex.SaveChangesAsync();
        }

        public async Task AddAsync(T entity)
        {
            _set.Add(entity);
            await _dbContex.SaveChangesAsync();
        }

        private DbContext _dbContex;
        private DbSet<T> _set;
    }
}
