﻿using System;
using GadgetBox.Rest.Features.EF.Mappings;
using Microsoft.EntityFrameworkCore;

namespace GadgetBox.Rest.Features.EF
{
    public class GadgetBoxDbContext : DbContext
    {
        public GadgetBoxDbContext()
        {
        }

        public GadgetBoxDbContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new DeviceCofiguration());
			modelBuilder.ApplyConfiguration(new BoxConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new HistoryConfiguration());
        }
    }
}
