using System;
using GadgetBox.Rest.Features.EF;
using GadgetBox.Rest.Infrastructure.Enums;
using Microsoft.EntityFrameworkCore;
using GadgetBox.Rest.Infrastructure.Models;

namespace GadgetBox.Rest
{
    public class PoCGadgetBoxDbContext : GadgetBoxDbContext
    {
        public PoCGadgetBoxDbContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>().HasData(
                new User { Id = 1, Name = "Yuliia Polyvka" , CardCode = "MHgwMDAwMDAwMDAwMDAwMQ==" },
                new User { Id = 2, Name = "Kostiatyn Kovalok", CardCode = "0000002" },
                new User { Id = 3, Name = "Serhii Mahera", CardCode = "0000003" },
                new User { Id = 4, Name = "Nazar Huliev", CardCode = "0000004" });

            modelBuilder.Entity<Box>().HasData(
                new Box { Id = 1, SlotsAmount = 2 });

            modelBuilder.Entity<Device>().HasData(
                new Device { Id = 1, OwnerId = null, BoxId = 1, Label = "Pixel 3", SlotIndex = 0, DeviceType = DeviceType.Phone, DeviceOs = DeviceOs.Android, Status=ReservationStatus.Free },
                new Device { Id = 2, OwnerId = null, BoxId = 1, Label = "iPhone XR", SlotIndex = 1, DeviceType = DeviceType.Phone, DeviceOs = DeviceOs.iOS, Status = ReservationStatus.Free });
            //Todo: comment before adding history
            //modelBuilder.Entity<DeviceHistory>().HasData(
            //    new DeviceHistory { Id = 1, UserId = 1, DeviceId = 1, Status = Infrastructure.Models.Enums.DeviceStatus.Booked, BookingTimeUtc = new DateTime(2019, 8, 8, 16, 0, 0) },
            //    new DeviceHistory { Id = 2, UserId = 1, DeviceId = 1, Status = Infrastructure.Models.Enums.DeviceStatus.Booked, BookingTimeUtc = new DateTime(2019, 8, 8, 18, 0, 0) });
        }
    }
}
