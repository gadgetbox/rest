using DeviceHive.Client;
using GadgetBox.DeviceHive;
using GadgetBox.Rest.Features.EF;
using GadgetBox.Rest.Features.IoT;
using GadgetBox.Rest.Infrastructure.DB;
using GadgetBox.Rest.Infrastructure.Options;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace GadgetBox.Rest
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) => ConfigureGadgetBoxServices(services, Configuration);

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseMvc();
        }


        private static void ConfigureGadgetBoxServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "GadgetBox.Rest API", Version = "v1" });
            });
            var bookingSection = configuration.GetSection("Booking");

            services.Configure<BookingConfig>(bookingSection);

            services.AddSingleton(new DeviceSemaphoreConfig(new System.Threading.Semaphore(0, 1)));

            ConfigureEF(services);

            ConfigureDeviceHive(services, configuration);

            
        }

        private static void ConfigureEF(IServiceCollection services)
        {
            services.AddDbContext<DbContext, PoCGadgetBoxDbContext>(options =>
            {
                options.UseInMemoryDatabase("InMemory");
            });

            services.AddTransient(typeof(IRepository<>), typeof(SimpleRepository<>));
        }

        private static void ConfigureDeviceHive(IServiceCollection services, IConfiguration configuration)
        {
            var deviceHiveSection = configuration.GetSection("DeviceHive");

            services.Configure<DeviceHiveConfig>(deviceHiveSection);

            var dhConfig = deviceHiveSection.Get<DeviceHiveConfig>();

            var connectionInfo = new DeviceHiveConnectionInfo(dhConfig.Rest);

            services.AddDeviceHiveClient(connectionInfo)
                .AddHttpMessageHandler(sp =>
                {
                    var options = dhConfig;

                    return new TokenAuthenticatedHandler(
                        options.RefreshToken,
                        options.Rest);
                });

            services.AddTransient<RC522Device>(sp => new RC522Device(
                dhConfig.DeviceId,
                sp.GetService<IDeviceHiveClient>()));

            services.AddTransient<SolenoidDevice>(sp => new SolenoidDevice(
               dhConfig.DeviceId,
               sp.GetService<IDeviceHiveClient>()));

            services.AddTransient<IHostedService>(sp =>
                new CenfermentBackgroundService(
                    sp.GetService<RC522Device>(),
                    sp.GetService<IServiceScopeFactory>(),
                    sp.GetService<SolenoidDevice>(),
                    sp.GetService<DeviceSemaphoreConfig>())
                {
                    Logger = sp.GetService<ILogger<CenfermentBackgroundService>>()
                });
        }
    }
}
