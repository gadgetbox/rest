﻿using System;
using GadgetBox.Rest.Infrastructure.Enums;

namespace GadgetBox.Rest.Infrastructure.Models.Extensions
{
    public static class DeviceExtensions
    {
        public static bool IsBooked(this Device device, Int32 bookingDurationInSeconds)
        {
            return device.Status == ReservationStatus.Booked && device.TimeUtc.AddSeconds(bookingDurationInSeconds) > DateTime.UtcNow;
        }

        public static bool IsUsed(this Device device)
        {
            return device.Status == ReservationStatus.InUse || device.Status == ReservationStatus.WaitForReturning;
        }
    }
}
