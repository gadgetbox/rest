﻿using System;
namespace GadgetBox.Rest.Infrastructure.Models
{
    public class Box : ModelBase
    {
        public Int32 SlotsAmount { get; set; }
    }
}
