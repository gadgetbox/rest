﻿using System;
namespace GadgetBox.Rest.Infrastructure.Models
{
    public class User : ModelBase
    {
        public String Name { get; set; }
        public String CardCode { get; set; }
    }
}
