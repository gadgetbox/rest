﻿using System;
using GadgetBox.Rest.Infrastructure.Enums;

namespace GadgetBox.Rest.Infrastructure.Models
{
    public class History : ModelBase
    {
        public DateTime TimeUtc { get; set; }
        public Int32 UserId { get; set; }
        public Int32 DeviceId { get; set; }
        public string Action { get; set; }

        public static History Create(Int32 userId, Int32 deviceId, string action)
        {
            return new History()
            {
                TimeUtc = DateTime.UtcNow,
                Action = action,
                DeviceId = deviceId,
                UserId = userId
            };
        }
    }
}
