﻿using System;
using GadgetBox.Rest.Infrastructure.Enums;

namespace GadgetBox.Rest.Infrastructure.Models
{
    public class Device : ModelBase
    {
        public String Label { get; set; }
        public Int32? OwnerId { get; set; }
        public Int32 BoxId { get; set; }
        public Int32 SlotIndex { get; set; }
        public DeviceType DeviceType { get; set; }
        public DeviceOs DeviceOs { get; set; }
        public DateTime TimeUtc { get; set; }
        public ReservationStatus Status { get; set; }
        public string Additionalinfo { get; set; }
    }
}