﻿using System;
using GadgetBox.Rest.Infrastructure.DB;

namespace GadgetBox.Rest.Infrastructure.Models
{
    public class ModelBase : IModel
    {
        public Int32 Id { get; set; }
    }
}
