﻿using System;
namespace GadgetBox.Rest.Infrastructure.DB
{
    public interface IModel
    {
        Int32 Id { get; set; }
    }
}
