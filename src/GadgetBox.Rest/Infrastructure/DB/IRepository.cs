﻿using System;
using System.Threading.Tasks;
using GadgetBox.Rest.Infrastructure.Models;

namespace GadgetBox.Rest.Infrastructure.DB
{
    public interface IRepository<T>
        where T : IModel
    {
        Task<T[]> GetAllAsync();
        Task<T> GetAsync(int id);
        Task UpdateAsync(T entity);
        Task AddAsync(T entity);
    }
}
