﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace GadgetBox.Rest.Infrastructure.API
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class ApiControllerBase : ControllerBase
    {
        
    }
}
