﻿namespace GadgetBox.Rest.Infrastructure.Enums
{
    public enum DeviceOs
    {
        iOS,
        Android,
        Other
    }
}