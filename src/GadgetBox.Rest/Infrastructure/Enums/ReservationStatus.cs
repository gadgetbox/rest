﻿namespace GadgetBox.Rest.Infrastructure.Enums
{
    public enum ReservationStatus
    {
        Free,
        Booked,
        InUse,
        WaitForReturning
    }
}
