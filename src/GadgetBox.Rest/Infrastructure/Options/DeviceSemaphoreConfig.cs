﻿using System;
using System.Threading;

namespace GadgetBox.Rest
{
    public class DeviceSemaphoreConfig
    {
        public DeviceSemaphoreConfig(Semaphore semaphore)
        {
            Semaphore = semaphore;
        }

        public Semaphore Semaphore { get; }
    }
}
