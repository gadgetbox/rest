﻿using System;
namespace GadgetBox.Rest.Infrastructure.Options
{
    public class DeviceHiveConfig
    {
        public String DeviceId { get; set; }
        public String Rest { get; set; }
        public String RefreshToken { get; set; }
    }
}
