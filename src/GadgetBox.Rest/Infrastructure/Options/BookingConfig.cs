﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GadgetBox.Rest.Infrastructure.Options
{
    public class BookingConfig
    {
        public Int32 BookingTimeInSeconds { get; set; }
        public Int32 OpenTimeInSeconds { get; set; }
    }
}
